package com.test.repository;

import com.test.model.Roles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jodanpotasu on 07.12.16.
 */
@Repository
public interface RolesRepository extends CrudRepository<Roles,Integer> {
}

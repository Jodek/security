package com.test.controller;

import com.test.model.Role;
import com.test.model.Roles;
import com.test.model.User;
import com.test.model.UserRole;
import com.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Set;

/**
 * Created by jodanpotasu on 07.12.16.
 */
@RequestMapping("/super")
@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "create", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public User create(String username){


        User temp = new User();
        temp.setUsername(username);
        temp.setPassword(username);
        temp.setActive(false);
        //temp.setUserRole();
        return userRepository.save(temp);
    }
}

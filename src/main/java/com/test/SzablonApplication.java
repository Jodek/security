package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SzablonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SzablonApplication.class, args);
	}
}

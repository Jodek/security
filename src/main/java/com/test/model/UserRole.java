package com.test.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "UserRole")
public class UserRole {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userRole")
    private Set<Roles> roles;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userRole")
    private Set<User> users;

    public UserRole(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }
}

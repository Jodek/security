package com.test.service;

import com.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserServiceDetailsImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;
//    private UserRepository userRepository;
//
//    public SSUserDetailsService(UserRepository userRepository){
//        this.userRepository=userRepository;
//    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByName(username);
    }
}
